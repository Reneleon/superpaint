document.addEventListener( 'deviceready', function() {
    let canvaspaint = new Paint(window.innerWidth,window.innerHeight-15,"paint")


    $("#cam").click(_=>{
        alert("cam");
        navigator.camera.getPicture(_=>{
            alert("ok")
        },_=>{
            alert("fehler")
        },{
            targetWidth:200,
            targetHeighz:200,
            correctOrientation:true
        });
    });

});

class Paint {
    constructor(width, height, idLocation) {
        // canvasinit
        this.height = height;
        this.width = width;
        this.canvas = document.createElement("CANVAS");
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.ctx = this.canvas.getContext("2d");
        this.lastX = 0;
        this.lastY = 0;

        document.getElementById(idLocation).appendChild(this.canvas);
        // canvasinit


        this.canvas.addEventListener('touchstart', (e) => {
            [this.lastX, this.lastY] = this.getXY(e);
        });
        this.canvas.addEventListener('touchmove',  this.draw.bind(this)  );
   

    }
    draw(e){

        this.ctx.beginPath();
        this.ctx.moveTo(this.lastX, this.lastY);
        this.ctx.lineTo(e.touches[0].clientX, e.touches[0].clientY-13);
        this.ctx.lineWidth = 2;
        this.ctx.stroke();
        [this.lastX, this.lastY] = this.getXY(e);
      }

      getXY(e){
          return [e.touches[0].clientX, e.touches[0].clientY-13]
      }

  }